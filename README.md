Demo Project:
Create server and deploy application on DigitalOcean
Technologies used:
DigitalOcean, Linux, Java, Gradle
Project Description:
Setup and configure a server on DigitalOcean
Create and configure a new Linux user on the Droplet
(Security best practice)
Deploy and run a Java Gradle application on Droplet
===================================================================================================

Create server on Digital server(Cloud Service Provider)
Steps : 
    Click on Droplet ->
        Select region,
        Choose and image/machine:Ubuntu, 
        Choose Authentication Method-Choose your SSH keys{copy from terminal : cat ~/.ssh/id_rsa.pub }, Finally click om create button
After creating Droplet Need to setup Firewall rule:
Steps:
    Click on Networking on left side panel:
        Give name to firewall eg: my-droplet:firewall
        Inbound Rules :  Type:SSH 	Protocol:TCP 	PortRange:22 	Sources:my_ipeddress{google:my-ipdresss:->copy} 
        Outbound Rules:
    Now click on create firewall rule

server got created on DigitalOcean

Connect to server:
    $ssh root@ipaddrees_droplet
    root@ubuntu-s-1vcpu-512mb-10gb-sgp1-01:~#apt update
    root@ubuntu-s-1vcpu-512mb-10gb-sgp1-01:~#apt install openjdk-8-jre-headless

gitlab:Pycharm:Terminal:Gradle
    $git clone git@gitlab.com:twn-devops-bootcamp/latest/05-cloud/java-react-example.git

    create blanck project:java_react_example
    checkout my git project
    git clone git@gitlab.com:devops_nana2/java-react-example.git
    copy to my java_react_exmaple from to my java-react-example
    Sadhanas-MacBook-Pro:java-react-example sanvi$ git status
    Sadhanas-MacBook-Pro:java-react-example sanvi$ git add -u
    Sadhanas-MacBook-Pro:java-react-example sanvi$ git commit -m "copied nana_project"
    Sadhanas-MacBook-Pro:java-react-example sanvi$ git push
    
Build Jar File
    Sadhanas-MacBook-Pro:java-react-example sanvi$ gradle build

Deploy On server
    Sadhanas-MacBook-Pro:java-react-example sanvi$ scp build/libs/java-react-example.jar root@143.198.86.139:/root

Run/Deploy On server(detached Mode)
    root@ubuntu-s-1vcpu-512mb-10gb-sgp1-01:~# java -jar java-react-example.jar &
        application is running on port 7071 , port is not so we need to open , DigitalOcean>Myfirewall>Inbound Rules:Custom 	TCP 	     7071 	All IPv4 All IPv6 ,save

Go to browser :
    droplet_ip_add:port(application_port:7071)

now we can see UI of our application
Check that our application is running
    root@ubuntu-s-1vcpu-512mb-10gb-sgp1-01:~# ps aux | grep java

Check on which port application running
    root@ubuntu-s-1vcpu-512mb-10gb-sgp1-01:~# netstat -lpnt {PID, Port}


Create and configure a Linux user on a cloud server 
add user
    root@ubuntu-s-1vcpu-512mb-10gb-sgp1-01:~# adduser san
    root@ubuntu-s-1vcpu-512mb-10gb-sgp1-01:~# usermod -aG sudo san
switch user
    root@ubuntu-s-1vcpu-512mb-10gb-sgp1-01:~# su - san
exit:
    san@ubuntu-s-1vcpu-512mb-10gb-sgp1-01:~$ exit
        logout
    root@ubuntu-s-1vcpu-512mb-10gb-sgp1-01:~# exit
        logout
    Connection to 143.198.86.139 closed.

    $ : standerd root user
    # : linux user

Connect to droplet (server)
    Sadhanas-MacBook-Pro:~ sanvi$ ssh root@143.198.86.139

Connected to root user
    root@ubuntu-s-1vcpu-512mb-10gb-sgp1-01:~# pwd
        /root
Switch user
    root@ubuntu-s-1vcpu-512mb-10gb-sgp1-01:~# su - san
    san@ubuntu-s-1vcpu-512mb-10gb-sgp1-01:~$ pwd
        /home/san

Try to sshing to Server with "san" user
    Sadhanas-MacBook-Pro:~ sanvi$ ssh san@143.198.86.139
        san@143.198.86.139: Permission denied (publickey).
    Permission denied so we need to configured authorized ssh public key for this user , which one given to root user.

Sadhanas-MacBook-Pro:~ sanvi$ ssh root@143.198.86.139

root@ubuntu-s-1vcpu-512mb-10gb-sgp1-01:~# su - san

Opne new terminal to take public ssh key 
Sadhanas-MacBook-Pro:.ssh sanvi$ cat ~/.ssh/id_rsa.pub 
copy key 
Now we need to go back to Server

We are in home directory
san@ubuntu-s-1vcpu-512mb-10gb-sgp1-01:~$ pwd
    /home/san
There is no .ssh folder
san@ubuntu-s-1vcpu-512mb-10gb-sgp1-01:~$ ls -a
.  ..  .bash_history  .bash_logout  .bashrc  .cloud-locale-test.skip  .profile

We need to create it 
san@ubuntu-s-1vcpu-512mb-10gb-sgp1-01:~$ mkdir .ssh
Now we need to create authorized_key file , we can paste public ssh key here 
san@ubuntu-s-1vcpu-512mb-10gb-sgp1-01:~$ sudo vim .ssh/authorized_key

Now we can connect to our own user not as root user

Sadhanas-MacBook-Pro:~ sanvi$ ssh san@143.198.86.139
